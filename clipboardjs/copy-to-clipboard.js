var clipboard = new ClipboardJS('.copy-to-clipboard');

clipboard.on('success', function(e) {
  // Create a popup element
  const popup = document.createElement('div');
  popup.classList.add('popup');
  popup.innerHTML = `
    <div class="popup-title">Copied! 😊</div>
  `;

  // Append the popup to the body
  document.body.appendChild(popup);

  // Calculate position of the trigger element
  const rect = e.trigger.getBoundingClientRect();
  popup.style.left = `${rect.left + window.scrollX}px`;
  popup.style.top = `${rect.bottom + window.scrollY}px`;

  // Show the popup with a fade-in effect
  setTimeout(() => {
    popup.classList.add('show');
  }, 10);

  // Function to hide the popup after 5 seconds
  setTimeout(function() {
    // Fade-out effect
    popup.classList.remove('show');
    // Remove the popup from the DOM after fade-out
    setTimeout(() => {
        popup.remove();
    }, 300); // Match the transition duration
  }, 2000);

  e.clearSelection();
});