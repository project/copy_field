# Copy field machine name

The Copy field machine name module provides the ability to copy the machine name of
a content type field. When creating custom themes or rendering
content type data, we often need to copy the content type field's machine name.
With this module, you can easily copy a field's machine name
with a single click. You can install this module in
a development environment by configuring it with Config Split.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/copy_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/copy_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit the content type you want to add copy field feature and
   edit the content type page admin/structure/types/manage/article,
   here you can enable or disable copy field feature.
1. After enabling this you can see a new tab Manage Copy field on
   admin/structure/types/manage/article.
1. Copy field machine name to clipboard and past it wherever you want.


## Maintainers

- Sanket Prajapati - [Sanket Prajapati](https://www.drupal.org/u/sanketaddweb)
