<?php

namespace Drupal\copy_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CopyFieldListingController to list entity type fields.
 */
class CopyFieldListingController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The module extenstion list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $modulePath;

  /**
   * Constructs a new CopyFieldController.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_path
   *   The Module path.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager = NULL, ModuleExtensionList $module_path) {
    $this->entityFieldManager = $entity_field_manager;
    $this->modulePath = $module_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('extension.list.module')
    );
  }

  /**
   * The copyField for copy field machine name.
   */
  public function copyField($node_type) {
    if ($node_type instanceof NodeTypeInterface) {
      $entity_type = 'node';
      $bundle = $node_type->id();
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      $field_list = [];

      foreach ($fields as $fieldName => $field) {
        if (strpos($fieldName, 'field_') === 0) {
          $field_list[$fieldName] = $field->getLabel();
        }
      }
    }

    // Table headers.
    $header = [
      'label' => 'Label',
      'machine_name' => 'Machine name',
    ];

    $module_path = $this->modulePath->getPath('copy_field');

    // Table rows.
    $rows = [];
    foreach ($field_list as $key => $value) {
      $rows[] = [
        'label' => $value,
        'machine_name' => $this->t("
          <span id='copy_@key'>@key</span>
          <a class='copy-to-clipboard' data-clipboard-target='#copy_@key' title='Click to copy'>
            <img src='/@module_path/image/clippy.svg' class='clippy_img' alt='Copy to clipboard'/>
          </a>",
          ['@key' => $key, '@module_path' => $module_path]),
      ];
    }

    // Build the table.
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => 'No data available',
    ];

    // Return the table as a render array.
    return [
      '#theme' => 'table',
      '#attributes' => ['class' => ['copy-field-wrapper']],
      '#header' => $table['#header'],
      '#rows' => $table['#rows'],
      '#empty' => $table['#empty'],
    ];
  }

}
