<?php

namespace Drupal\copy_field\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks access based on a configuration setting.
 */
class ConfigConditionCheck implements AccessInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ConfigConditionCheck.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Checks access based on the configuration setting.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The node type.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access($node_type) {
    $editable_config_form = "node.type." . $node_type;
    $config = $this->configFactory->get($editable_config_form);
    $access_result = $config->get('use_copy_field') ? AccessResult::allowed() : AccessResult::forbidden();

    // Create a CacheableMetadata object and set max-age to 0.
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(0);

    // Attach the cache metadata to the access result.
    return $access_result->addCacheableDependency($cache_metadata);
  }

}
